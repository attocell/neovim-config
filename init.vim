" leader key
let mapleader = ","

" vim plug
call plug#begin('~/.vim/plugged/')

Plug 'morhetz/gruvbox'
Plug 'itchyny/lightline.vim'
" Plug 'terryma/vim-multiple-cursors'

call plug#end()

""""""""""""""""""""
" general settings
""""""""""""""""""""
set nocompatible

" relative numbers
set number relativenumber

" don't make swap file
set noswapfile

" ignore casing when searching, but enable when input contains upper case letter
set ignorecase smartcase

" mark the line where the the cursor is
set cursorline

" linebreak
set linebreak

set tabstop=4
" highlight mathing pairs, eg. (), [], {} ...
set matchpairs+=<:>
set hlsearch

" fuzzy finder with tab completion
set path+=**
set wildmenu

" ignore certain filetypes
set wildignore=*.dvi,*.pdf,*.jpg,*.png,*.aux,*.log,*.ps,*.fls,*.fdb_latexmk

" disable auto indentation
filetype indent off

" save automatically when swithing buffers
set autowrite

" automatically change working directory to current file
set autochdir

set clipboard+=unnamedplus

" windows navigation
nnoremap <C-j> <C-w><C-j>
nnoremap <C-k> <C-w><C-k>
" nnoremap <C-l> <C-w><C-l>
" nnoremap <C-h> <C-w><C-h>
nnoremap <C-q> <C-w><C-q>

" colors
syntax enable
set background=dark
let base16colorspace=256
colorscheme gruvbox
set termguicolors
" colorscheme default


""""""""""""""""""""
" netrw
""""""""""""""""""""
" toggle netrw
nnoremap <silent> <leader>e :Lexplore<cr>

" make netrw behave like nerdtree
let g:netrw_banner = 0
let g:netrw_liststyle = 4
let g:netrw_browse_split = 4
let g:netrw_altv = 1
let g:netrw_winsize = 25


""""""""""""""""""""
" neovim config
""""""""""""""""""""
" mapping for opening neovim config 
nnoremap <leader>vrc :e ~/.config/nvim/init.vim<cr>

""""""""""""""""""""
" latex
""""""""""""""""""""
" read latex files as 'tex' rather than 'plaintex'
autocmd BufRead,BufNewFile *.tex set filetype=tex
" compile and open pdf in pdf viewer
autocmd FileType tex map <leader>p :silent !latexmk -pdf -pv %<CR>
" compile
autocmd FileType tex map <leader>c :silent !latexmk -pdf %<CR>
" automatically clear build files when leaving a latex document
autocmd VimLeave *.tex !latexmk -c

" snippets
autocmd FileType tex inoremap ,beg \begin{}
autocmd FileType tex inoremap ,it \textit{} <Esc>T{i


""""""""""""""""""""
" python
""""""""""""""""""""
" compile
autocmd FileType python map <leader>c :!python %<CR>
